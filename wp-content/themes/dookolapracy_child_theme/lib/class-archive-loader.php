<?php


class archive_loader
{

    public $query = 'not set';

    /**
     * archive_loader constructor.
     */
    public function __construct()
    {

    }

    /**
     * archive_loader constructor.
     */
    public function get_posts_per_page()
    {
        $default_posts_per_page = get_option('posts_per_page');
        return $default_posts_per_page;
    }

    /**
     * archive_loader constructor.
     */
    public function load_more()
    {
        header('Content-type: text/html');

        $load_more_main_query = json_decode(base64_decode( $_POST['q']), true);

        $queries = $load_more_main_query['queries'][0];

        $args = array(
            'posts_per_page' => archive_loader::get_posts_per_page(),
            'category_name' => $queries['terms'][0],
            'orderby' => 'date',
            'order' => 'DESC',
            'post__not_in' => explode(',', $_POST['b']),
        );
        $posts_array = get_posts($args);
        $have_posts = is_array($posts_array) && !empty($posts_array);
        global $post, $kt_post_with_sidebar;
        $kt_post_with_sidebar = true;

        if ($have_posts) {
            $max = count($posts_array);
            for ($i = 0; $i < $max; $i++) {
                $post = $posts_array[$i];

                get_template_part('templates/content', get_post_format($post));
            }

        } else {
        }

        exit;
    }

    /**
     * archive_loader constructor.
     */
    public static function load_more_button_html()
    {
        global $archive_loader_instance;


        ?>
        <button id="load-more-archive-posts" class="load-more-archive-posts kad-animation"
                data-query="<?php echo base64_encode($archive_loader_instance->query ); ?>"
                data-action-url="<?php echo admin_url('admin-ajax.php'); ?>">Chcę
        więcej</button><?php
    }

    /**
     * Removes std theme pagination
     */
    public function remove_theme_pagination()
    {
        if (is_category()) {
            remove_action('virtue_pagination', 'virtue_pagination_markup', 20);
            add_action('virtue_pagination', 'archive_loader::load_more_button_html', 20);
        }
    }

    /**
     * Removes std theme pagination
     */
    public function enqueue_scripts()
    {
        wp_enqueue_script('archive-loa4343der3-js', get_stylesheet_directory_uri() . '/assets/js/scripts-archive-loader.js', ['jquery'], '1.0', true);
    }


    /**
     * @param $query
     */
    public static function setup_archives_category_query($query)
    {
        if ($query->is_archive() && $query->is_main_query() && !is_admin()) {
            $query->set('posts_per_page', archive_loader::get_posts_per_page());

            global $archive_loader_instance;

            $archive_loader_instance->query = json_encode($query->tax_query);
            //var_dump($archive_loader_instance);

            //set_transient('load_more_main_query', json_encode($query->tax_query), 0);
        }
    }

}

$archive_loader_instance = new archive_loader();
/*
add_action('pre_get_posts', 'archive_loader::setup_archives_category_query');
add_action('template_redirect', 'archive_loader::remove_theme_pagination');

add_action('wp_ajax_load_more', 'archive_loader::load_more');
add_action('wp_ajax_nopriv_load_more', 'archive_loader::load_more');

add_action('wp_enqueue_scripts', 'archive_loader::enqueue_scripts');
*/