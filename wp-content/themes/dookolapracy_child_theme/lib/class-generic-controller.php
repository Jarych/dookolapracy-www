<?php
/**
 * Created by PhpStorm.
 * User: Gaad
 * Date: 2018-10-29
 * Time: 21:06
 */


class generic_view_controller
{

    public $query = 'not set';

    /**
     * generic_view_controller constructor.
     */
    public function __construct()
    {

    }

    /**
     * generic_view_controller constructor.

    /**
     * Removes std theme pagination
     */
    public function enqueue_scripts()
    {
        wp_enqueue_script('generic-view-controller-js', get_stylesheet_directory_uri() . '/assets/js/generic-view-controller.js', ['jquery'], '1.0', true);
    }



}

$generic_view_controller_instance = new generic_view_controller();



add_action('wp_enqueue_scripts', 'generic_view_controller::enqueue_scripts');