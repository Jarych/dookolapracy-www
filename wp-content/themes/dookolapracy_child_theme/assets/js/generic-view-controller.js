(function ($, w, d) {
    /**
     * Controller constructor
     *
     * change this name when using this as a template
     */
    var generic_view_controller = function () {

        this.state = {GDPR_agree: false};

        this.valid = false;

        /**
         * Controller name
         * change this name when using this as a template. Constructor name and this value should match
         */
        this.controllerName = "generic_view_controller";


        this.row_z_postami_img_height = function () {

            if ($('body').is('.home')) {
                var items = $('#row_z_postami .grid-items').children();
                var max_height = 0;

                for (var i = 0; i < items.length; i++) {
                    var item = $(items[i]);
                    var layer_media = item.find('.layer-media');
                    var layer_media_h = layer_media.height();
                    max_height = max_height < layer_media_h ? layer_media_h : max_height;
                }
                for (var i = 0; i < items.length; i++) {
                    item = $(items[i]);
                    layer_media = item.find('.layer-media');
                    var img = item.find('img');
                    var img_src = img.attr('src');
                    if ( typeof img_src !== "undefined") {

                        layer_media.css({
                            backgroundPosition: 'center',
                            backgroundImage: 'url('+ img_src +')',
                            backgroundSize: 'cover',
                            height: max_height
                        });
                    } else {
                        console.log('img-src undefined');
                    }
                    img.remove();
                }

            }
        };

        this.page_home__ready = function () {

            this.row_z_postami_img_height();
            $(document).on('resize', w.generic_view_controller.row_z_postami_img_height);
        };


        /**
         * on /kategorie page it was a problem with a submenu that have two active elements.
         * also those menu items was connected to corresponding categories buttons
         */
        this.submenu_active_reset = function(){
            $("#menu-dookola_submenu .current-menu-item").removeClass("current-menu-item");

            var marker = 0;

            if( $('button[data-filter]:contains("Zmiana zawodowa")').is('.selected') ){
                $("#menu-dookola_submenu").find("li").eq(0).addClass("current-menu-item");
            marker=1;
            }
            if( $('button[data-filter]:contains("Ścieżki kariery")').is('.selected') ){
                $("#menu-dookola_submenu").find("li").eq(2).addClass("current-menu-item");
            marker=2;
            }

            if( marker === 0){
                $("#menu-dookola_submenu .current-menu-item").removeClass("current-menu-item");
            }


        };

        /**
         * Selecting categories by url hash
          */
        this.page_kategorie_btn_click = function(){
            if ($('body').is('.page-chce-zmienic-prace') || $('body').is('.page-szukam-pomyslu-na-siebie')) {
                this.state.page_kategorie_btn_click_interval = setInterval( this.selectGridCategory )
            }
        };

        this.selectGridCategory = function () {
            var $isotopeHolder = $('.the-post-grid_category').find('.tpg-isotope');
            var $isotope = $isotopeHolder.find('.rt-tpg-isotope');
            if (typeof $isotope.imagesLoaded === "function"){

                var hashString = new String(decodeURIComponent(window.location.hash)).replace('#', '');
                if ( hashString.length === 0 ) return;

                var button = jQuery('button:contains("' + hashString + '")');
                var filterValue = $(button).attr('data-filter');
                if ( typeof $isotope.isotope !== "undefined" ){
                    $isotope.isotope({filter: filterValue});
                    button.parent().find('.selected').removeClass('selected');
                    button.addClass('selected');

                    clearInterval(window.generic_view_controller.state.page_kategorie_btn_click_interval);
                }
            }
        };

this.page_kategorie__ready = function () {
            w.generic_view_controller.page_kategorie_btn_click();
            w.generic_view_controller.submenu_active_reset();
        };

        this.document__ready = function () {
            w.generic_view_controller.page_home__ready();

            w.generic_view_controller.page_kategorie__ready();
            w.onhashchange = w.generic_view_controller.page_kategorie__ready;
        };

        /**
         * Listeners declarations
         */
        this.addListeners = function () {
            $(document).ready(this.document__ready);
        };

        /**
         *  Reinitializes the controller
         */
        this.reInit = function () {
            var initializedStrName = this.controllerName + '_initialized';
            window[initializedStrName] = false;
            this.init();
        };

        /**
         * everything that needs to be auto loaded with controller functions
         */
        this.init = function () {
            var initializedStrName = this.controllerName + '_initialized';

            window[initializedStrName] = window[initializedStrName] || false;
            if (!window[initializedStrName]) {
                window[initializedStrName] = true;
                window[this.controllerName] = this;

                // controller initialization
                this.addListeners();


            }
            return this;
        };

        return this.init();

    };

    //instantiate controller
    var controller = new generic_view_controller();


})(jQuery, window, document);


