(function ($, w, d) {

    var load_more_controller = function () {

        this.controllerName = "load_more_controller";

        this.state = {};

        this.post__not_in = function () {
            var curArticles = jQuery('article[id^="post"]');
            var curIds = [];
            for (var i = 0; i < curArticles.length; i++) {
                if ($(curArticles[i]).is('[id]'))
                    curIds.push(curArticles[i].id.replace('post-', ''));
            }
            return curIds.join(',');
        };

        this.appendSpinner = function () {
            var spinner = $('<div class="load-more-spinner-wrapper"><i class="load-more-spinner fa fa-spinner fa-spin fa-3x fa-fw"></i></div>');
            this.spinner = spinner;
            $('.postlist .kt_archivecontent').append(spinner);
        };

        this.disableSpinner = function () {
            $('.load-more-spinner-wrapper').remove();
        };

        this.loadMoreBtn__click = function (e) {
            var url = $(e.currentTarget).data('action-url');
            var query = $(e.currentTarget).data('query');
            data = {
                action: 'load_more',
                q: query,
                b: window.load_more_controller.post__not_in()
            };

            window.load_more_controller.appendSpinner();

            $.ajax({
                url: url,
                type: 'post',
                data: data,
                success: function (response) {
                    window.load_more_controller.disableSpinner();

                    var art = $(response).filter('article');
                    $('.postlist .kt_archivecontent').append(art);
                    art.each(function () {
                        $(this).animate({'opacity': 1, 'top': 0});
                    });
                }
            });
        };

        this.addListeners = function () {
            //window.addEventListener( 'click',  )
            $('#load-more-archive-posts').on('click', this.loadMoreBtn__click);
        };

        /**
         * everything that needs to be auto loaded with controller functions
         */
        this.init = function () {
            var initializedStrName = this.controllerName + '_initialized';

            window[initializedStrName] = window[initializedStrName] || false;
            if (!window[initializedStrName]) {
                window[initializedStrName] = true;
                window[this.controllerName] = this;

                // controller initialization
                this.addListeners();
            }
            return this;
        };

        return this.init();


    };


//instantiate controller
    var load_more = new load_more_controller();


})(jQuery, window, document);


function setSliderTitleActive(api) {

    var slideIndex = jQuery(api.currentSlide.$element).index();

    var animL = jQuery('.ms-overlay-layers .ms-anim-layers');
    if (animL.is('*')) {

        //animL.children().removeClass( 'active' ).filter( '[data-action="gotoSlide('+slideIndex+')"]' ).addClass( 'active' );
        animL.children().removeClass('active');
        animL.children().eq(slideIndex).addClass('active');
    }



}
