<?php


if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

$archive_filename_fallback = 'archive-product.php';
$_path                     = get_stylesheet_directory() . '/woocommerce/';
$queried_object            = get_queried_object();
if ( is_object( $queried_object ) ) {
    $custom_archive_filename = 'archive-product-' . $queried_object->slug . '.php';
}

if ( is_file( $_path . $custom_archive_filename ) )
    wc_get_template( $custom_archive_filename );
else wc_get_template( $archive_filename_fallback );
